var cart = angular.module('shopApp', ['number']);

var base_url = 'http://localhost:8888/project/gangfy-cart/';

cart.controller('commonController', function ($scope, $http, $window) {

    $scope.multival = {
        name: {
            minlength: 7,
            maxlength: 7
        }
    };
    $scope.IsVisible = false;
    $scope.errmsg = false;
    $scope.qtymsg = false;
    $scope.fradmsg = false;

    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth() + 1;
    if (month < 10) {
        month = "0" + month;
    }
    var day = d.getDate();
    if (day < 10) {
        day = "0" + day;
    }
    $scope.date = year + "-" + month + "-" + day;

    $scope.cntry = [{name: 'Select Country', value: ''}];

    $scope.getCountry = function (dfl) {
        $http({
            method: 'get',
            url: base_url + 'data/get-country',
            headers: {'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            if (response.data) {
                for (var a = 0; a < response.data.length; a++) {
                    $scope.cntry.push({name: response.data[a]['country'], value: response.data[a]['id']});
                }
                if (dfl) {
                    $scope.country = $scope.cntry[0].value;
                }
            }
        });
    };

    $scope.getBusiness = function () {
        $http({
            method: 'get',
            url: base_url + 'business/get-business',
            headers: {'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            if (response.data) {
                $scope.getCountry();
                $scope.bname = response.data[0]['business_name'];
                $scope.regnum = response.data[0]['reg_no'];
                $scope.cname = response.data[0]['company_name'];
                $scope.country = response.data[0]['country_id'];
                $scope.phoneg = response.data[0]['phone_general'];
                $scope.phonem = response.data[0]['phone_mobile'];
                $scope.addone = response.data[0]['address_one'];
                $scope.addtwo = response.data[0]['address_two'];
                if (response.data[0]['business_logo']) {
                    $scope.uplogo = base_url + response.data[0]['business_logo'];
                    $scope.shwimg = 'true';
                }

            }
        });
    };

    $scope.ubsnss = [{name: 'Select Business', value: ''}];
    $scope.userBusiness = function (dfl) {
        $http({
            method: 'get',
            url: base_url + 'stores/get-business',
            headers: {'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            if (response.data) {
                for (var b = 0; b < response.data.length; b++) {
                    $scope.ubsnss.push({name: response.data[b]['business_name'], value: response.data[b]['id']});
                }
                if (dfl) {
                    $scope.bname = $scope.ubsnss[0].value;
                }
            }
        });
    };

    $scope.strtyp = [{name: 'Select Store Type', value: ''}];
    $scope.storeTypes = function (dfl) {
        $http({
            method: 'get',
            url: base_url + 'data/store-types',
            headers: {'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            if (response.data) {
                for (var c = 0; c < response.data.length; c++) {
                    $scope.strtyp.push({name: response.data[c]['store_type'], value: response.data[c]['id']});
                }
                if (dfl) {
                    $scope.stype = $scope.strtyp[0].value;
                }
            }
        });
    };

    $scope.scale = [{name: 'Select Scale', value: ''}];
    $scope.getScale = function (dfl) {
        $http({
            method: 'get',
            url: base_url + 'data/get-scale',
            headers: {'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            if (response.data) {
                for (var d = 0; d < response.data.length; d++) {
                    $scope.scale.push({name: response.data[d]['scale'], value: response.data[d]['id']});
                }
                if (dfl) {
                    $scope.scl = $scope.scale[0].value;
                }
            }
        });
    };

    $scope.ustore = [{name: 'Select Stores', value: ''}];
    $scope.userStores = function (dfl) {
        $http({
            method: 'get',
            url: base_url + 'products/get-stores',
            headers: {'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            if (response.data) {
                for (var e = 0; e < response.data.length; e++) {
                    $scope.ustore.push({name: response.data[e]['store_name'], value: response.data[e]['id']});
                }
                if (dfl) {
                    $scope.strs = $scope.ustore[0].value;
                }
            }
        });
    };

    $scope.psts = [{name: 'Select Status', value: ''}];
    $scope.productStatus = function (dfl) {
        $http({
            method: 'get',
            url: base_url + 'data/products-status',
            headers: {'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            if (response.data) {
                for (var f = 0; f < response.data.length; f++) {
                    $scope.psts.push({name: response.data[f]['status'], value: response.data[f]['id']});
                }
                if (dfl) {
                    $scope.pstatus = $scope.psts[0].value;
                }
            }
        });
    };

    $scope.crrncy = [{name: 'Select Currency', value: ''}];
    $scope.getCurrency = function (dfl) {
        $http({
            method: 'get',
            url: base_url + 'data/get-currency',
            headers: {'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            if (response.data) {
                for (var g = 0; g < response.data.length; g++) {
                    $scope.crrncy.push({name: response.data[g]['code'], value: response.data[g]['id']});
                }
                if (dfl) {
                    $scope.currency = $scope.crrncy[0].value;
                }
            }
        });
    };


    $scope.getStore = function () {
        $http({
            method: 'get',
            url: base_url + 'stores/get-store',
            headers: {'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            if (response.data) {
                $scope.userBusiness();
                $scope.storeTypes();
                $scope.bname = response.data[0]['business_id'];
                $scope.sname = response.data[0]['store_name'];
                $scope.stype = response.data[0]['st_id'];
                $scope.pac = response.data[0]['store_location'];
                $scope.latbox = response.data[0]['latitude'];
                $scope.lngbox = response.data[0]['longitude'];
            }
        });
    };

    $scope.uprdcts = [];
    $scope.getProducts = function () {
        $http({
            method: 'get',
            url: base_url + 'products/get-products',
            headers: {'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            if (response.data) {

                for (var h = 0; h < response.data.length; h++) {
                    var st = response.data[h]['pstatus_id'];
                    var btn = '';
                    if (st === '12') {
                        btn = 'btn-success';
                    } else if (st === '13') {
                        btn = 'btn-warning';
                    } else if (st === '14' || st === '15') {
                        btn = 'btn-danger';
                    }

                    var prdcts = {
                        id: parseInt($scope.uprdcts.length + 1),
                        pid: response.data[h]['id'],
                        strname: response.data[h]['store_name'],
                        prname: response.data[h]['product_name'],
                        scale: response.data[h]['scale'],
                        qty: response.data[h]['quantity'],
                        price: response.data[h]['price'] + ' ' + response.data[h]['code'],
                        status: response.data[h]['status'],
                        stbtn: btn
                    };
                    $scope.uprdcts.push(prdcts);
                }
                $scope.productStatus('true');
            }
        });
    };

    $scope.updateStatus = function (pid, sid) {
        $http({
            method: 'post',
            url: base_url + 'products/update-status',
            headers: {'Content-Type': 'application/json'},
            data: JSON.stringify({pid: pid,
                sid: sid})
        }).then(function successCallback(response) {
            if (response.data === 'success') {
                $scope.uprdcts = [];
                $scope.getProducts();
                $scope.psts = [{name: 'Select Status', value: ''}];
            }
        });
    };

    $scope.getProduct = function () {
        $http({
            method: 'get',
            url: base_url + 'products/get-product',
            headers: {'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            if (response.data) {
                $scope.userStores();
                $scope.getScale();
                $scope.getCurrency();
                $scope.productStatus();
                $scope.strs = response.data[0]['store_id'];
                $scope.pname = response.data[0]['product_name'];
                $scope.scl = response.data[0]['scale_id'];
                $scope.qty = response.data[0]['quantity'];
                $scope.mqty = response.data[0]['min_quantity'];
                $scope.currency = response.data[0]['currency_id'];
                $scope.price = response.data[0]['price'];
                $scope.pstatus = response.data[0]['pstatus_id'];
                $scope.editor = response.data[0]['description'];
                if (response.data[0]['image']) {
                    $scope.primg = base_url + response.data[0]['image'];
                    $scope.shwimg = 'true';
                }
            }
        });
    };

});


