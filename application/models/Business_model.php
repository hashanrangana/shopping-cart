<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Business_model extends CI_Model {

    function exist_business($name) {
        $this->db->where('business_name', $name);
        $query = $this->db->get('business');
        $data = $query->result_array();
        return $data;
    }

    function add_business($name, $regno, $cname, $cid, $phoneg, $phonem, $addone, $addtwo, $logo, $uid) {
        $this->db->set('business_name', $name);
        $this->db->set('reg_no', $regno);
        $this->db->set('company_name', $cname);
        $this->db->set('country_id ', $cid);
        $this->db->set('phone_general ', $phoneg);
        $this->db->set('phone_mobile ', $phonem);
        $this->db->set('address_one ', $addone);
        $this->db->set('address_two ', $addtwo);
        $this->db->set('business_logo ', $logo);
        $this->db->set('user_id ', $uid);
        $this->db->insert('business');
    }

    function all_business($id) {
        $this->db->where('bs.user_id', $id);
        $this->db->where('st.level', 1);
        $this->db->select('bs.id,bs.business_name,bs.company_name,cnt.country,bs.phone_general,bs.address_one,bs.address_two,st.status');
        $this->db->join('status AS st', 'st.id = bs.status_id');
        $this->db->join('country AS cnt', 'cnt.id = bs.country_id');
        $query = $this->db->get('business AS bs');
        $data = $query->result_array();
        return $data;
    }

    function get_business($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('business');
        $data = $query->result_array();
        return $data;
    }

    function update_business($id, $name, $regno, $cname, $cid, $phoneg, $phonem, $addone, $addtwo, $logo) {
        $this->db->where('id', $id);
        $data['business_name'] = $name;
        $data['reg_no'] = $regno;
        $data['company_name'] = $cname;
        $data['country_id'] = $cid;
        $data['phone_general'] = $phoneg;
        $data['phone_mobile'] = $phonem;
        $data['address_one'] = $addone;
        $data['address_two'] = $addtwo;
        $data['business_logo'] = $logo;
        $this->db->update('business', $data);
    }

}
