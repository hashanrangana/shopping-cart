<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model {

    function get_stores($id) {
        $this->db->where('user_id', $id);
        $this->db->select('id,store_name');
        $query = $this->db->get('stores');
        $data = $query->result_array();
        return $data;
    }

    function exist_product($name, $id) {
        $this->db->where('product_name', $name);
        $this->db->where('user_id', $id);
        $query = $this->db->get('products');
        $data = $query->result_array();
        return $data;
    }

    function add_product($sid, $pname, $scale, $qty, $mqty, $cid, $price, $pid, $dsc, $image, $uid) {
        $this->db->set('store_id', $sid);
        $this->db->set('product_name', $pname);
        $this->db->set('scale_id', $scale);
        $this->db->set('quantity ', $qty);
        $this->db->set('min_quantity ', $mqty);
        $this->db->set('currency_id ', $cid);
        $this->db->set('price ', $price);
        $this->db->set('pstatus_id ', $pid);
        $this->db->set('description ', $dsc);
        $this->db->set('image ', $image);
        $this->db->set('user_id ', $uid);
        $this->db->insert('products');
    }

    function get_products($id) {
        $this->db->where('prd.user_id', $id);
        $this->db->select('prd.id,str.store_name,prd.product_name,scl.scale,prd.quantity,cr.code,prd.price,prd.pstatus_id,st.status');
        $this->db->join('status AS st', 'st.id = prd.pstatus_id');
        $this->db->join('currency AS cr', 'cr.id = prd.currency_id');
        $this->db->join('scale AS scl', 'scl.id = prd.scale_id');
        $this->db->join('stores AS str', 'str.id = prd.store_id');
        $query = $this->db->get('products AS prd');
        $data = $query->result_array();
        return $data;
    }

    function update_status($id, $sid) {
        $this->db->where('id', $id);
        $data['pstatus_id'] = $sid;
        $this->db->update('products', $data);
    }

    function get_product($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('products');
        $data = $query->result_array();
        return $data;
    }

    function update_product($id,$sid, $pname, $scale, $qty, $mqty, $cid, $price, $pid, $dsc, $image) {
        $this->db->where('id', $id);
        $data['store_id'] = $sid;
        $data['product_name'] = $pname;
        $data['scale_id'] = $scale;
        $data['quantity'] = $qty;
        $data['min_quantity'] = $mqty;
        $data['currency_id'] = $cid;
        $data['price'] = $price;
        $data['pstatus_id'] = $pid;
        $data['description'] = $dsc;
        $data['image'] = $image;
        $this->db->update('products', $data);
    }

}
