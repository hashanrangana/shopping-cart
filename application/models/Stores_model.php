<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stores_model extends CI_Model {

    function get_business($id) {
        $this->db->where('user_id', $id);
        $this->db->select('id,business_name');
        $query = $this->db->get('business');
        $data = $query->result_array();
        return $data;
    }

    function exist_store($bid, $sname, $stype) {
        $this->db->where('business_id', $bid);
        $this->db->where('store_name', $sname);
        $this->db->where('st_id', $stype);
        $query = $this->db->get('stores');
        $data = $query->result_array();
        return $data;
    }

    function add_store($bid, $sname, $stype, $loc, $lat, $lng, $region, $uid) {
        $this->db->set('business_id', $bid);
        $this->db->set('store_name', $sname);
        $this->db->set('st_id', $stype);
        $this->db->set('store_location ', $loc);
        $this->db->set('latitude ', $lat);
        $this->db->set('longitude ', $lng);
        $this->db->set('region ', $region);
        $this->db->set('user_id ', $uid);
        $this->db->insert('stores');
    }

    function all_stores($id) {
        $this->db->where('str.user_id', $id);
        $this->db->where('st.level', 1);
        $this->db->select('str.id,bs.business_name,str.store_name,sty.store_type,str.store_location,st.status');
        $this->db->join('status AS st', 'st.id = str.status_id');
        $this->db->join('business AS bs', 'bs.id = str.business_id');
        $this->db->join('store_types AS sty', 'sty.id = str.st_id');
        $query = $this->db->get('stores AS str');
        $data = $query->result_array();
        return $data;
    }

    function get_store($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('stores');
        $data = $query->result_array();
        return $data;
    }

    function update_store($id, $bid, $sname, $stype, $loc, $lat, $lng, $region,$dstr) {
        $this->db->where('id', $id);
        $data['business_id'] = $bid;
        $data['store_name'] = $sname;
        $data['st_id'] = $stype;
        $data['store_location'] = $loc;
        $data['latitude'] = $lat;
        $data['longitude'] = $lng;
        $data['region'] = $region;
        $data['district'] = $dstr;
        $this->db->update('stores', $data);
    }

}
