<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Data_model extends CI_Model {

    function get_country() {
        $this->db->select('id,country');
        $query = $this->db->get('country');
        $data = $query->result_array();
        return $data;
    }

    function store_types() {
        $this->db->where('status', 1);
        $this->db->select('id,store_type');
        $query = $this->db->get('store_types');
        $data = $query->result_array();
        return $data;
    }

    function get_scale() {
        $this->db->where('status', 1);
        $this->db->select('id,scale');
        $query = $this->db->get('scale');
        $data = $query->result_array();
        return $data;
    }

    function products_status() {
        $this->db->where('level', 2);
        $this->db->select('id,status');
        $query = $this->db->get('status');
        $data = $query->result_array();
        return $data;
    }

    function get_currency() {
        $this->db->where('status', 1);
        $this->db->select('id,code');
        $query = $this->db->get('currency');
        $data = $query->result_array();
        return $data;
    }

}
