<?php $this->load->view('includes/header'); ?>
<!-- ============================================================== -->
<?php $this->load->view('includes/message'); ?>
<!-- ============================================================== -->
<div class="content-page" ng-app="shopApp">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">GangFy</h4>

                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Products</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>products/list-products">List Products</a></li>
                            <li class="breadcrumb-item active">Edit Product</li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <div class="row" ng-controller="commonController">

                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12" data-ng-init="getProduct()">
                                <?php echo form_open_multipart('products/update-product', array('class' => 'form-horizontal', 'name' => 'fvForm')); ?>
                                <h4 class="header-title m-t-0 m-b-30">Update Product</h4>
                                <fieldset>
                                    <legend>Product Information:</legend>
                                    <div class="row"> 
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="strs">Store Name *</label>
                                                <select  class="custom-select" id="strs" name="strs" ng-model="strs" ng-options="option.value as option.name for option in ustore" ng-required="true"></select>                                  
                                                <span ng-show="fvForm.strs.$dirty && fvForm.strs.$error.required"></span>
                                                <span ng-hide="fvForm.strs.$invalid"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="pname">Product Name *</label>
                                                <input type="text" class="form-control" id="pname" name="pname" ng-model="pname" placeholder="Product Name" ng-required="true">
                                                <span class="fa fa-times" id="clear" ng-show="fvForm.pname.$dirty && fvForm.pname.$error.required"></span>
                                                <span class="fa fa-check" id="dok" ng-hide="fvForm.pname.$invalid"></span>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Product Details:</legend>
                                    <div class="row"> 
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label for="scl">Scale *</label>
                                                <select  class="custom-select" id="scl" name="scl" ng-model="scl" ng-options="option.value as option.name for option in scale" ng-required="true"></select>                                  
                                                <span ng-show="fvForm.scl.$dirty && fvForm.scl.$error.required"></span>
                                                <span ng-hide="fvForm.scl.$invalid"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label for="qty">Quantity (How you sell product) *</label>
                                                <input type="text" class="form-control" id="qty" name="qty" ng-model="qty" number placeholder="1 Item, 1 Kg, 250 Grams, 1 Ltr or 500 Ml" ng-required="true">
                                                <span class="fa fa-times" id="clear" ng-show="fvForm.qty.$dirty && fvForm.qty.$error.required"></span>
                                                <span class="fa fa-check" id="dok" ng-hide="fvForm.qty.$invalid"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label for="mqty">Minimum Quantity per Order *</label>
                                                <input type="text" class="form-control" id="qty" name="mqty" ng-model="mqty" number placeholder="Minimum Quantity per Order" ng-required="true">
                                                <span class="fa fa-times" id="clear" ng-show="fvForm.mqty.$dirty && fvForm.mqty.$error.required"></span>
                                                <span class="fa fa-check" id="dok" ng-hide="fvForm.mqty.$invalid"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label for="currency">Currency *</label>
                                                <select  class="custom-select" id="currency" name="currency" ng-model="currency" ng-options="option.value as option.name for option in crrncy" ng-required="true"></select>                                  
                                                <span ng-show="fvForm.currency.$dirty && fvForm.currency.$error.required"></span>
                                                <span ng-hide="fvForm.currency.$invalid"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label for="price">Price *</label>
                                                <input type="text" class="form-control" id="price" name="price" ng-model="price" number placeholder="Price" ng-required="true">
                                                <span class="fa fa-times" id="clear" ng-show="fvForm.price.$dirty && fvForm.price.$error.required"></span>
                                                <span class="fa fa-check" id="dok" ng-hide="fvForm.price.$invalid"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label for="pstatus">Status *</label>
                                                <select  class="custom-select" id="pstatus" name="pstatus" ng-model="pstatus" ng-options="option.value as option.name for option in psts" ng-required="true"></select>                                  
                                                <span ng-show="fvForm.pstatus.$dirty && fvForm.pstatus.$error.required"></span>
                                                <span ng-hide="fvForm.pstatus.$invalid"></span>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Product Description:</legend>
                                    <div class="row"> 
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <label for="editor">Product Description (Not Required)</label>
                                                <textarea id="editor1" name="editor" style="border:none;" ng-model="editor"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Product Image:</legend>
                                    <div class="row"> 
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="pimage">Upload your product image ( Image size should be under (W 1024 * H 768) )</label>  
                                                <input type="file" class="form-control" id="files" name="userfile" title="Image size should be under (W 1024 * H 768)" ng-model="pimage"> 
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <img ng-src="{{primg}}" class="business-logo" ng-show="shwimg"> 
                                                <label for="logo" ng-show="!shwimg" class="no-img">Still you don't have uploaded logo</label> 
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 button-group">
                                        <div class="form-group">
                                            <input type="submit" name="save" ng-disabled="!fvForm.$valid" class="btn btn-success" title="Please fill the form correctly to enable the button" value="Update Product">
                                            <a href="<?php echo base_url(); ?>products/list-products" title="Close" class="btn btn-warning">Close</a>                
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div> <!-- container -->
</div>
<!-- content -->
</div>
<!-- End content-page -->
<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>

<script>
                                                CKEDITOR.replace('editor1', {
                                                    toolbar: [
                                                        {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript']},
                                                        {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                                                        {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                                                        {name: 'links', groups: ['links']},
                                                        {name: 'insert', groups: ['insert']}
                                                    ]

                                                });

</script>

<?php $this->load->view('includes/footer'); ?>