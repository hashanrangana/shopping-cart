<?php $this->load->view('includes/header'); ?>
<!-- ============================================================== -->
<?php $this->load->view('includes/message'); ?>
<!-- ============================================================== -->
<div class="content-page" ng-app="shopApp">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">GangFy</h4>

                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Products</a></li>
                            <li class="breadcrumb-item active">List Product </li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <div class="row" ng-controller="commonController">

                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">

                                <h4 class="header-title m-t-0 m-b-30">List Products</h4>

                                <fieldset>
                                    <legend>List Products:</legend>
                                    <div class="row">
                                        <div class="col-md-12" data-ng-init="getProducts()">
                                            <table id="search" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Store Name</th>
                                                        <th>Product Name</th>
                                                        <th>Scale</th>
                                                        <th>Quantity</th>      
                                                        <th>Price</th> 
                                                        <th>Status</th> 
                                                        <th>Quick Action</th> 
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr ng-repeat="prd in uprdcts track by $index">
                                                        <td>{{prd.strname}}</td>
                                                        <td>{{prd.prname}}</td>
                                                        <td>{{prd.scale}}</td>
                                                        <td>{{prd.qty}}</td>
                                                        <td>{{prd.price}}</td>
                                                        <td><span class="btn btn-sm {{prd.stbtn}}">{{prd.status}}</span></td>   
                                                        <td><select  class="custom-select" id="pstatus" name="pstatus" ng-model="pstatus" ng-options="option.value as option.name for option in psts" ng-change="updateStatus(prd.pid, pstatus)"></select></td>
                                                        <td><a href="<?php echo base_url(); ?>products/edit/{{prd.pid}}" class="btn btn-info btn-sm" title="Edit"><i class="fa fa-pencil"></i></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div> <!-- container -->
</div>
<!-- content -->
</div>
<!-- End content-page -->



<?php $this->load->view('includes/footer'); ?>