<?php $this->load->view('includes/header'); ?>
<!-- ============================================================== -->
<?php $this->load->view('includes/message'); ?>
<!-- ============================================================== -->
<div class="content-page" ng-app="shopApp">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">GangFy</h4>

                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Business</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>business/list-business">Manage Business</a></li>
                            <li class="breadcrumb-item active">Edit Business</li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <div class="row" ng-controller="commonController">

                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12" data-ng-init="getBusiness()">
                                <?php echo form_open_multipart('business/update-business', array('class' => 'form-horizontal', 'name' => 'fvForm')); ?>
                                <h4 class="header-title m-t-0 m-b-30">Update Business</h4>
                                <fieldset>
                                    <legend>Business Information:</legend>
                                    <div class="row"> 
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="bname">Business Name *</label>
                                                <input type="text" class="form-control" id="bname" name="bname" ng-model="bname" placeholder="Business Name" ng-required="true">   
                                                <span class="fa fa-times" id="clear" ng-show="fvForm.bname.$dirty && fvForm.bname.$error.required"></span>
                                                <span class="fa fa-check" id="dok" ng-hide="fvForm.bname.$invalid"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="regnum">Registration Number</label>
                                                <input type="text" class="form-control" id="regnum" name="regnum" ng-model="regnum" placeholder="Registration Number">                       
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="cname">Company Name *</label>
                                                <input type="text" class="form-control" id="cname" name="cname" ng-model="cname" placeholder="Company Name if Business Name" ng-required="true">   
                                                <span class="fa fa-times" id="clear" ng-show="fvForm.cname.$dirty && fvForm.cname.$error.required"></span>
                                                <span class="fa fa-check" id="dok" ng-hide="fvForm.cname.$invalid"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="country">Country *</label>                                  
                                                <select  class="custom-select" id="cname" name="country" ng-model="country" ng-options="option.value as option.name for option in cntry" ng-required="true"></select>                                  
                                                <span ng-show="fvForm.country.$dirty && fvForm.country.$error.required"></span>
                                                <span ng-hide="fvForm.country.$invalid"></span>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Contact Information:</legend>
                                    <div class="row"> 
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="phoneg">Phone Number (General) *</label>
                                                <input type="text" class="form-control" id="phoneg" name="phoneg" ng-model="phoneg" number placeholder="Phone Number (General)" ng-required="true">   
                                                <span class="fa fa-times" id="clear" ng-show="fvForm.phoneg.$dirty && fvForm.phoneg.$error.required"></span>
                                                <span class="fa fa-check" id="dok" ng-hide="fvForm.phoneg.$invalid"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="phonem">Phone Number (Mobile) *</label>
                                                <input type="text" class="form-control" id="phonem" name="phonem" ng-model="phonem" number placeholder="Phone Number (Mobile)" ng-required="true">   
                                                <span class="fa fa-times" id="clear" ng-show="fvForm.phonem.$dirty && fvForm.phonem.$error.required"></span>
                                                <span class="fa fa-check" id="dok" ng-hide="fvForm.phonem.$invalid"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="addone">Address Line One *</label>
                                                <input type="text" class="form-control" id="addone" name="addone" ng-model="addone" placeholder="Address Line One" ng-required="true">   
                                                <span class="fa fa-times" id="clear" ng-show="fvForm.addone.$dirty && fvForm.addone.$error.required"></span>
                                                <span class="fa fa-check" id="dok" ng-hide="fvForm.addone.$invalid"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="addtwo">Address Line Two</label>
                                                <input type="text" class="form-control" id="addtwo" name="addtwo" ng-model="addtwo" placeholder="Address Line Two">    
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Business Logo:</legend>
                                    <div class="row"> 
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="logo">Upload your logo here</label>  
                                                <input type="file" class="form-control" id="files" name="userfile" ng-model="logo"> 
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <img ng-src="{{uplogo}}" class="business-logo" ng-show="shwimg"> 
                                                <label for="logo" ng-show="!shwimg" class="no-img">Still you don't have uploaded logo</label> 
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 button-group">
                                        <div class="form-group">
                                            <input type="submit" name="save" ng-disabled="!fvForm.$valid" class="btn btn-success" title="Please fill the form correctly to enable the button" value="Update">
                                            <a href="<?php echo base_url(); ?>business/list-business" title="Close" class="btn btn-warning">Close</a>                
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div> <!-- container -->
</div>
<!-- content -->
</div>
<!-- End content-page -->



<?php $this->load->view('includes/footer'); ?>