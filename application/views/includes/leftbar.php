
<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>
                <!--                <li class="text-muted menu-title">Side Bar</li>-->

                <li class="has_sub">
                    <a href="<?php echo base_url(); ?>" class="waves-effect"><i
                            class="zmdi zmdi-view-dashboard"></i><span> Dashboard </span> </a>
                </li>
                <li class="has_sub">
                    <a class="waves-effect <?php
                    if (isset($bsact)) {
                        echo $bsact;
                    }
                    ?>"><i class="zmdi zmdi-home"></i>
                        <span> Business </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url(); ?>business/create">Create Business</a></li>
                        <li <?php
                        if (isset($bsact)) {
                            echo 'class="' . $bsact . '"';
                        }
                        ?>><a href="<?php echo base_url(); ?>business/list-business">Manage Business</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a class="waves-effect <?php
                    if (isset($stact)) {
                        echo $stact;
                    }
                    ?>"><i class="zmdi zmdi-store"></i>
                        <span> Stores </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url(); ?>stores/create">Create Store</a></li>
                        <li <?php
                        if (isset($stact)) {
                            echo 'class="' . $stact . '"';
                        }
                        ?>><a href="<?php echo base_url(); ?>stores/list-stores">Manage Stores</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a class="waves-effect <?php
                    if (isset($pract)) {
                        echo $pract;
                    }
                    ?>"><i class="zmdi zmdi-collection-item"></i>
                        <span> Products </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url(); ?>products/create">Add Product</a></li>
                        <li <?php
                        if (isset($pract)) {
                            echo 'class="' . $pract . '"';
                        }
                        ?>><a href="<?php echo base_url(); ?>products/list-products">Manage Products</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a class="waves-effect"><i class="zmdi zmdi-truck"></i>
                        <span> Delivery Fees </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <!--<li><a href="<?php echo base_url(); ?>">Sales</a></li>-->
                    </ul>
                </li>
                <li class="has_sub">
                    <a class="waves-effect"><i class="zmdi zmdi-money-box"></i>
                        <span> Sales </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <!--<li><a href="<?php echo base_url(); ?>">Sales</a></li>-->
                    </ul>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->
