<!-- Top Bar Start -->
<div class="topbar">
    <!-- LOGO -->
    <div class="topbar-left">
<!--        <a href="<?php echo base_url(); ?>" class="logo">
            <i class="icon-c-logo"></i>
            <span>Apexaura</span></a>-->
        <a href="<?php echo base_url(); ?>" class="logo">
            <!--<img style="width: auto;height: 70px;" src="<?php echo base_url(); ?>assets/images/logo/dashboard.png">-->
            <span>GangFY</span></a>
    </div>
    <nav class="navbar-custom">
        <ul class="list-inline float-right mb-0">

            <li class="list-inline-item  notification-list arrow-expand-list">
                <i class="ion-arrow-expand noti-icon" onclick="toggleFullScreen()"></i>
            </li>
            <li class="list-inline-item dropdown notification-list">
                <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown"
                   href="#" role="button"
                   aria-haspopup="false" aria-expanded="false">
                    <i class="zmdi zmdi-notifications-none noti-icon"></i>
                    <span class="noti-icon-badge"></span>
                </a>
<!--                <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg" aria-labelledby="Preview">
                     item
                    <div class="dropdown-item noti-title">
                        <h5>
                            <small><span class="label label-danger pull-xs-right">7</span>Notification</small>
                        </h5>
                    </div>
                     item
                    <a href="#" class="dropdown-item notify-item">
                        <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                        <p class="notify-details">commented on Admin
                            <small class="text-muted">1min ago</small>
                        </p>
                    </a>
                     item
                    <a href="#" class="dropdown-item notify-item">
                        <div class="notify-icon bg-info"><i class="icon-user"></i></div>
                        <p class="notify-details">New user registered.
                            <small class="text-muted">1min ago</small>
                        </p>
                    </a>
                     item
                    <a href="#" class="dropdown-item notify-item">
                        <div class="notify-icon bg-danger"><i class="icon-like"></i></div>
                        <p class="notify-details">Carlos Crouch liked <b>Admin</b>
                            <small class="text-muted">1min ago</small>
                        </p>
                    </a>
                     All
                    <a href="#" class="dropdown-item notify-item notify-all">
                        View All
                    </a>
                </div>-->
            </li>

            <li class="list-inline-item dropdown notification-list">
                <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown"
                   href="#" role="button"
                   aria-haspopup="false" aria-expanded="false">
                    <img src="<?php echo base_url(); ?>assets/images/users/avatar-4.jpg" alt="user" class="rounded-circle">
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                    <!-- item-->
                    <div class="dropdown-item noti-title">
                        <h5 class="text-overflow">
                            <small>Welcome <?php
                                if ($this->session->userdata('identity')) {
                                    echo $this->session->userdata('identity');
                                }
                                ?></small>
                        </h5>
                    </div>

                    <!-- item-->
                    <a href="#" class="dropdown-item notify-item">
                        <i class="zmdi zmdi-account-circle"></i> <span>Profile</span>
                    </a>

                    <!-- item-->
                    <a href="#" class="dropdown-item notify-item">
                        <i class="zmdi zmdi-settings"></i> <span>Settings</span>
                    </a>

                    <!-- item-->
                    <a href="<?php echo base_url(); ?>auth/logout" class="dropdown-item notify-item">
                        <i class="zmdi zmdi-power"></i> <span>Logout</span>
                    </a>

                </div>
            </li>
        </ul>

        <ul class="list-inline menu-left mb-0">
            <li class="float-left">
                <button class="button-menu-mobile open-left waves-light waves-effect">
                    <i class="zmdi zmdi-menu"></i>
                </button>
            </li>
<!--            <li class="hidden-mobile app-search">
                <form role="search" class="">
                    <input type="text" placeholder="Search..." class="form-control">
                    <a href=""><i class="fa fa-search"></i></a>
                </form>
            </li>-->
        </ul>
    </nav>
</div>
<!-- Top Bar End -->