<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <!--<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/logo/favicon.ico">-->

        <!-- App title -->
        <title>GangFY | <?php
            if (isset($title)) {
                echo $title;
            }
            ?></title>

        <!-- Switchery css -->
        <link href="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <!-- Bootstrap CSS -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Plugins css -->
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

        <?php if (isset($table)) { ?>
            <!-- Data table css -->
            <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
            <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <?php } ?>
        <!-- App CSS -->
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css" />
        <?php if (isset($map)) { ?>
            <script>

                function initMap() {
                    var input = document.getElementById('pac-input');
                    // var inputpac = document.getElementById('pac');
                    var options = {
                        types: ['(cities)'],
                    };

                    var geocoder = new google.maps.Geocoder();

                    map = new google.maps.Map(document.getElementById('searchMap'), {
                        center: {lat: 0, lng: 0},
                        zoom: 12
                    });
                    geocoder.geocode({'address': ""}, function (results, status) {
                        if (status === 'OK') {
                            map.setCenter(results[0].geometry.location);
                        } else {
                            //   alert('Geocode was not successful for the following reason: ' + status);
                        }
                    });

                    var autocomplete = new google.maps.places.Autocomplete(input, options);
                    // Bind the map's bounds (viewport) property to the autocomplete object,
                    // so that the autocomplete requests use the current map bounds for the
                    // bounds option in the request.




                    autocomplete.addListener('place_changed', function () {
                        var place = autocomplete.getPlace();

                        if (!place.geometry) {
                            // User entered the name of a Place that was not suggested and
                            // pressed the Enter key, or the Place Details request failed.
                            window.alert("No details available for input: '" + place.name + "'");
                            return;
                        }


                        var address = '';
                        if (place.address_components) {
                            address = [
                                (place.address_components[0] && place.address_components[0].short_name || ''),
                                (place.address_components[1] && place.address_components[1].short_name || ''),
                                (place.address_components[2] && place.address_components[2].short_name || '')
                            ].join(' ');
                        }
                        map.setCenter(place.geometry.location);

                    });


                }
            </script>
        <?php } ?>
        <!-- Modernizr js -->
        <script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>


    </head>

    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">

            <?php $this->load->view('includes/topbar'); ?>

            <?php $this->load->view('includes/leftbar'); ?>

