<?php
if ($this->session->flashdata('err_msg')) {
    ?>
    <div class="sys-msg col-sm-5 alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert"
                aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>Sorry ! </strong><?php echo $this->session->flashdata('err_msg'); ?>
    </div>
    <?php
}
if ($this->session->flashdata('succ_msg')) {
    ?>
    <div class="sys-msg col-sm-5 alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert"
                aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <?php echo $this->session->flashdata('succ_msg'); ?>
    </div>
    <?php
}