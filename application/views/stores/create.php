<?php $this->load->view('includes/header'); ?>
<!-- ============================================================== -->
<?php $this->load->view('includes/message'); ?>
<!-- ============================================================== -->
<div class="content-page" ng-app="shopApp">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">GangFy</h4>

                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Stores</a></li>
                            <li class="breadcrumb-item active">Create Store</li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <div class="row" ng-controller="commonController">

                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                                <?php echo form_open('stores/create-store', array('class' => 'form-horizontal', 'name' => 'fvForm')); ?>
                                <h4 class="header-title m-t-0 m-b-30">Create Store</h4>
                                <fieldset>
                                    <legend>Store Information:</legend>
                                    <div class="row"> 
                                        <div class="col-md-4 col-sm-12" data-ng-init="userBusiness('true')">
                                            <div class="form-group">
                                                <label for="bname">Business Name *</label>
                                                <select  class="custom-select" id="bname" name="bname" ng-model="bname" ng-options="option.value as option.name for option in ubsnss" ng-required="true"></select>                                  
                                                <span ng-show="fvForm.bname.$dirty && fvForm.bname.$error.required"></span>
                                                <span ng-hide="fvForm.bname.$invalid"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label for="sname">Store Name *</label>
                                                <input type="text" class="form-control" id="sname" name="sname" ng-model="sname" placeholder="Store Name" ng-required="true">
                                                <span class="fa fa-times" id="clear" ng-show="fvForm.sname.$dirty && fvForm.sname.$error.required"></span>
                                                <span class="fa fa-check" id="dok" ng-hide="fvForm.sname.$invalid"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12" data-ng-init="storeTypes('true')">
                                            <div class="form-group">
                                                <label for="stype">Store Type *</label>
                                                <select  class="custom-select" id="stype" name="stype" ng-model="stype" ng-options="option.value as option.name for option in strtyp" ng-required="true"></select>                                  
                                                <span ng-show="fvForm.stype.$dirty && fvForm.stype.$error.required"></span>
                                                <span ng-hide="fvForm.stype.$invalid"></span>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Store Location:</legend>
                                    <div class="row"> 
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <label for="pac">Search Store Location on Google Map *</label>
                                                <input type="text" class="form-control" id="pac-input"  name="pac" ng-model="pac" ng-minlength="30" placeholder="Search Store Location on Google Map" ng-required="true">   
                                                <span class="fa fa-times" id="clear" ng-show="fvForm.pac.$dirty && fvForm.pac.$error.required"></span>
                                                <span class="fa fa-times" id="clear" ng-show="fvForm.pac.$error.minlength"></span>
                                                <span class="fa fa-check" id="dok" ng-hide="fvForm.pac.$invalid"></span>
                                            </div>
                                        </div>
                                        <div style="display:none;">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="hidden" class="form-control" id="latbox" name="latbox" ng-model="latbox">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="hidden" class="form-control " id="lngbox" name="lngbox" ng-model="lngbox">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="hidden" class="form-control " id="region" name="region">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="hidden" class="form-control " id="district" name="district">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="gb-map">
                                                <div id="map" style="height: 250px;"></div>    
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 button-group">
                                        <div class="form-group">
                                            <input type="submit" name="save" ng-disabled="!fvForm.$valid" class="btn btn-success" title="Please fill the form correctly to enable the button" value="Create">
                                            <a href="<?php echo base_url(); ?>" title="Close" class="btn btn-warning">Close</a>                
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div> <!-- container -->
</div>
<!-- content -->
</div>
<!-- End content-page -->

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpHZC2DR6DiK9TfSBO-6Xr_JcLwIe94so&libraries=places&callback=initAutocomplete"
async defer></script>
<script>
                                                function initAutocomplete() {
                                                    var map = new google.maps.Map(document.getElementById('map'), {
                                                        center: {lat: 43.7944337, lng: -79.6719619},
                                                        zoom: 8,
                                                        mapTypeId: 'roadmap'
                                                    });

                                                    // Create the search box and link it to the UI element.
                                                    var input = document.getElementById('pac-input');
                                                    var searchBox = new google.maps.places.SearchBox(input);
//        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                                                    // Bias the SearchBox results towards current map's viewport.
                                                    map.addListener('bounds_changed', function () {
                                                        searchBox.setBounds(map.getBounds());
                                                    });

                                                    var markers = [];
                                                    // Listen for the event fired when the user selects a prediction and retrieve
                                                    // more details for that place.
                                                    searchBox.addListener('places_changed', function () {
                                                        var places = searchBox.getPlaces();

                                                        if (places.length === 0) {
                                                            return;
                                                        }

                                                        // Clear out the old markers.
                                                        markers.forEach(function (marker) {
                                                            marker.setMap(null);
                                                        });
                                                        markers = [];

                                                        // For each place, get the icon, name and location.
                                                        var bounds = new google.maps.LatLngBounds();
                                                        places.forEach(function (place) {
                                                            if (!place.geometry) {
                                                                console.log("Returned place contains no geometry");
                                                                return;
                                                            }

                                                            var icon = {
                                                                url: place.icon,
                                                                size: new google.maps.Size(71, 71),
                                                                origin: new google.maps.Point(0, 0),
                                                                anchor: new google.maps.Point(17, 34),
                                                                scaledSize: new google.maps.Size(25, 25)
                                                            };

                                                            // Create a marker for each place.
                                                            markers.push(new google.maps.Marker({
                                                                map: map,
                                                                icon: icon,
                                                                title: place.name,
                                                                position: place.geometry.location
                                                            }));

                                                            document.getElementById("latbox").value = place.geometry.location.lat();
                                                            document.getElementById("lngbox").value = place.geometry.location.lng();

                                                            if (place.address_components.length === 4 || place.address_components.length === 5) {
                                                                $('#district').val(place.address_components[1].long_name);
                                                                $('#region').val(place.address_components[2].long_name);
                                                            } else if (place.address_components.length === 6) {
                                                                $('#district').val(place.address_components[2].long_name);
                                                                $('#region').val(place.address_components[3].long_name);
                                                            } else if (place.address_components.length === 7) {
                                                                $('#district').val(place.address_components[3].long_name);
                                                                $('#region').val(place.address_components[4].long_name);
                                                            } else if (place.address_components.length === 8) {
                                                                $('#district').val(place.address_components[4].long_name);
                                                                $('#region').val(place.address_components[5].long_name);
                                                            }

                                                            if (place.geometry.viewport) {
                                                                // Only geocodes have viewport.
                                                                bounds.union(place.geometry.viewport);
                                                            } else {
                                                                bounds.extend(place.geometry.location);
                                                            }
                                                        });
                                                        map.fitBounds(bounds);
                                                    });
                                                }

</script>

<?php $this->load->view('includes/footer'); ?>