<?php $this->load->view('includes/header'); ?>
<!-- ============================================================== -->
<?php $this->load->view('includes/message'); ?>
<!-- ============================================================== -->
<div class="content-page" ng-app="shopApp">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">GangFy</h4>

                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Stores</a></li>
                            <li class="breadcrumb-item active">List Stores </li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <div class="row" ng-controller="commonController">

                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">

                                <h4 class="header-title m-t-0 m-b-30">List Stores</h4>
                                <?php
                                if (isset($st)) {
                                    ?>
                                    <fieldset>
                                        <legend>List Stores:</legend>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table id="search" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Business Name</th>
                                                            <th>Store Name</th>
                                                            <th>Store Type</th>
                                                            <th>Store Location</th>      
                                                            <th>Status</th> 
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        foreach ($st as $data) {
                                                            $id = encrypt_url($data['id']);
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $data['business_name']; ?></td>
                                                                <td><?php echo $data['store_name']; ?></td>
                                                                <td><?php echo $data['store_type']; ?></td>
                                                                <td><?php echo $data['store_location']; ?></td>                                                                                                          
                                                                <td><?php echo $data['status']; ?></td> 
                                                                <td>
                                                                    <a href="<?php echo base_url(); ?>stores/edit/<?php echo $id; ?>" class="btn btn-info btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <?php
                                }
                                ?>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div> <!-- container -->
</div>
<!-- content -->
</div>
<!-- End content-page -->



<?php $this->load->view('includes/footer'); ?>