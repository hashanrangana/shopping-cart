<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    function get_country() {
        $data = $this->data->get_country();
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    function store_types() {
        $data = $this->data->store_types();
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    function get_scale() {
        $data = $this->data->get_scale();
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    function products_status() {
        $data = $this->data->products_status();
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }
    
    function get_currency() {
        $data = $this->data->get_currency();
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

}
