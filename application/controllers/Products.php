<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

    public $uid = 'arwghrr@gmail.com';

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function create() {
        $data['title'] = 'Create Store';
        $this->load->view('products/create', $data);
    }

    public function add_product() {
        $this->form_validation->set_rules('strs', 'Store Name', 'required');
        $this->form_validation->set_rules('pname', 'Product Name', 'required');
        $this->form_validation->set_rules('scl', 'Scale', 'required');
        $this->form_validation->set_rules('qty', 'Quntity', 'required');
        $this->form_validation->set_rules('mqty', 'Min Quntity', 'required');
        $this->form_validation->set_rules('currency', 'Currency', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('pstatus', 'Product Status', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('err_msg', 'Please fill all the required field');
            redirect('products/create');
        } else {
            $uid = $this->uid;
            $store = str_replace("string:", '', $this->input->post('strs'));
            $pname = $this->input->post('pname');
            $scale = str_replace("string:", '', $this->input->post('scl'));
            $qty = $this->input->post('qty');
            $mqty = $this->input->post('mqty');
            $curr = str_replace("string:", '', $this->input->post('currency'));
            $price = $this->input->post('price');
            $pstat = str_replace("string:", '', $this->input->post('pstatus'));
            $editor = $this->input->post('editor');
            $ext = $this->products->exist_product($pname, $uid);
            if (count($ext) == 0) {

                $eid = substr(sha1($uid), 0, 6);
                $img_path = wordwrap(strtolower($pname), 1, '-', 0);
                $path_one = './uploads/products/' . $eid . '/' . $img_path . '/';
                $path_rpl = str_replace(".", "", $path_one);
                $path_final = substr($path_rpl, 1);

                if (!file_exists($path_one)) {
                    mkdir($path_one, 0777, TRUE);
                }
                $config = array(
                    'upload_path' => $path_one,
                    'allowed_types' => "gif|jpg|png|jpeg",
                    'overwrite' => TRUE,
                    'max_size' => "2048",
                    'max_height' => "768",
                    'max_width' => "1024",
                    'overwrite' => TRUE
                );
                $path_name = '';
                $this->load->library('upload', $config);
                if ($this->upload->do_upload()) {
                    $data = $this->upload->data();
                    $path_name = $path_final . $data['file_name'];
                }

                $this->products->add_product($store, $pname, $scale, $qty, $mqty, $curr, $price, $pstat, $editor, $path_name, $uid);
                $this->session->set_flashdata('succ_msg', 'Product successfully saved');
                redirect('products/create');
            } else {
                $this->session->set_flashdata('err_msg', 'This Product already exists');
                redirect('products/create');
            }
        }
    }

    public function list_products() {
        $this->remove_session();
        $data['table'] = 'true';
        $this->load->view('products/list', $data);
    }

    public function edit() {
        $this->remove_session();
        $id = decrypt_url($this->uri->segment(3));
        $this->session->set_userdata('prid', $id);
        $data['pract'] = 'active';
        $this->load->view('products/edit', $data);
    }

    public function update_product() {
        $id = $this->session->userdata('prid');
        $enid = encrypt_url($id);
        if (!empty($id)) {
            $this->form_validation->set_rules('strs', 'Store Name', 'required');
            $this->form_validation->set_rules('pname', 'Product Name', 'required');
            $this->form_validation->set_rules('scl', 'Scale', 'required');
            $this->form_validation->set_rules('qty', 'Quntity', 'required');
            $this->form_validation->set_rules('mqty', 'Min Quntity', 'required');
            $this->form_validation->set_rules('currency', 'Currency', 'required');
            $this->form_validation->set_rules('price', 'Price', 'required');
            $this->form_validation->set_rules('pstatus', 'Product Status', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please fill all the required field');
                redirect('products/edit/' . $enid);
            } else {
                $uid = $this->uid;
                $store = str_replace("string:", '', $this->input->post('strs'));
                $pname = $this->input->post('pname');
                $scale = str_replace("string:", '', $this->input->post('scl'));
                $qty = $this->input->post('qty');
                $mqty = $this->input->post('mqty');
                $curr = str_replace("string:", '', $this->input->post('currency'));
                $price = $this->input->post('price');
                $pstat = str_replace("string:", '', $this->input->post('pstatus'));
                $editor = $this->input->post('editor');

                $eid = substr(sha1($uid), 0, 6);
                $img_path = wordwrap(strtolower($pname), 1, '-', 0);
                $path_one = './uploads/products/' . $eid . '/' . $img_path . '/';
                $path_rpl = str_replace(".", "", $path_one);
                $path_final = substr($path_rpl, 1);

                if (!file_exists($path_one)) {
                    mkdir($path_one, 0777, TRUE);
                }
                $config = array(
                    'upload_path' => $path_one,
                    'allowed_types' => "gif|jpg|png|jpeg",
                    'overwrite' => TRUE,
                    'max_size' => "2048",
                    'max_height' => "768",
                    'max_width' => "1024",
                    'overwrite' => TRUE
                );
                $path_name = '';
                $this->load->library('upload', $config);
                if ($this->upload->do_upload()) {
                    $data = $this->upload->data();
                    $path_name = $path_final . $data['file_name'];
                }

                $fpath = '';
                $pd = $this->products->get_product($id);
                if (!empty($path_name)) {
                    $fpath = $path_name;
                } else {
                    $fpath = $pd[0]['image'];
                }

                $this->products->update_product($id, $store, $pname, $scale, $qty, $mqty, $curr, $price, $pstat, $editor, $fpath);
                $this->session->set_flashdata('succ_msg', 'Product successfully updated');
                redirect('products/edit/' . $enid);
            }
        } else {
            $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
            redirect('products/list-products');
        }
    }

    function get_product() {
        $id = $this->session->userdata('prid');
        $data = $this->products->get_product($id);
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    function update_status() {
        $request = json_decode(file_get_contents('php://input'), TRUE);
        $pid = $request['pid'];
        $sid = $request['sid'];
        if (!empty($pid) && !empty($sid)) {
            $id = decrypt_url($pid);
            $this->products->update_status($id, $sid);
            $data = 'success';
        } else {
            $data = 'error';
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    function get_products() {
        $data = array();
        $prd = $this->products->get_products($this->uid);
        for ($a = 0; $a < count($prd); $a++) {
            $data[$a]['id'] = encrypt_url($prd[$a]['id']);
            $data[$a]['store_name'] = $prd[$a]['store_name'];
            $data[$a]['product_name'] = $prd[$a]['product_name'];
            $data[$a]['scale'] = $prd[$a]['scale'];
            $data[$a]['quantity'] = $prd[$a]['quantity'];
            $data[$a]['code'] = $prd[$a]['code'];
            $data[$a]['price'] = $prd[$a]['price'];
            $data[$a]['pstatus_id'] = $prd[$a]['pstatus_id'];
            $data[$a]['status'] = $prd[$a]['status'];
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    function get_stores() {
        $data = $this->products->get_stores($this->uid);
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    function remove_session() {
        $this->session->unset_userdata('prid');
    }

}
