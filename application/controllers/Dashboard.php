<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public $uid = 'arwghrr@gmail.com';

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index() {
        $data['title'] = 'Admin Panel';
        $this->load->view('dashboard/dashboard', $data);
    }

}
