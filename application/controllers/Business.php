<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends CI_Controller {

    public $uid = 'arwghrr@gmail.com';

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function create() {
        $data['title'] = 'Create Business';
        $this->load->view('business/create', $data);
    }

    public function create_business() {
        $this->form_validation->set_rules('bname', 'Business Name', 'required');
        $this->form_validation->set_rules('cname', 'Company Name', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('phoneg', 'Phone General', 'required');
        $this->form_validation->set_rules('phonem', 'Phone Mobile', 'required');
        $this->form_validation->set_rules('addone', 'Address One', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('err_msg', 'Please fill all the required field');
            redirect('business/create');
        } else {
            $uid = $this->uid;
            $bname = $this->input->post('bname');
            $regnum = $this->input->post('regnum');
            $cname = $this->input->post('cname');
            $country = str_replace("string:", '', $this->input->post('country'));
            $phoneg = $this->input->post('phoneg');
            $phonem = $this->input->post('phonem');
            $addone = $this->input->post('addone');
            $addtwo = $this->input->post('addtwo');
            $ext = $this->business->exist_business($bname);
            if (count($ext) == 0) {

                $eid = substr(sha1($uid), 0, 6);
                $img_path = wordwrap(strtolower($bname), 1, '-', 0);
                $path_one = './uploads/business/' . $eid . '/' . $img_path . '/';
                $path_rpl = str_replace(".", "", $path_one);
                $path_final = substr($path_rpl, 1);

                if (!file_exists($path_one)) {
                    mkdir($path_one, 0777, TRUE);
                }
                $config = array(
                    'upload_path' => $path_one,
                    'allowed_types' => "gif|jpg|png|jpeg",
                    'overwrite' => TRUE,
                    'max_size' => "2048",
                    'max_height' => "768",
                    'max_width' => "1024",
                    'overwrite' => TRUE
                );
                $path_name = '';
                $this->load->library('upload', $config);
                if ($this->upload->do_upload()) {
                    $data = $this->upload->data();
                    $path_name = $path_final . $data['file_name'];
                }

                $this->business->add_business($bname, $regnum, $cname, $country, $phoneg, $phonem, $addone, $addtwo, $path_name, $uid);
                $this->session->set_flashdata('succ_msg', 'Business successfully created');
                redirect('business/create');
            } else {
                $this->session->set_flashdata('err_msg', 'This Business already exists');
                redirect('business/create');
            }
        }
    }

    public function list_business() {
        $this->remove_session();
        $data['table'] = 'true';
        $data['bs'] = $this->business->all_business($this->uid);
        $this->load->view('business/list', $data);
    }

    public function edit() {
        $this->remove_session();
        $id = decrypt_url($this->uri->segment(3));
        $this->session->set_userdata('bsid', $id);
        $data['bsact'] = 'active';
        $this->load->view('business/edit', $data);
    }

    public function update_business() {
        $id = $this->session->userdata('bsid');
        $enid = encrypt_url($id);
        if (!empty($id)) {
            $this->form_validation->set_rules('bname', 'Business Name', 'required');
            $this->form_validation->set_rules('cname', 'Company Name', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');
            $this->form_validation->set_rules('phoneg', 'Phone General', 'required');
            $this->form_validation->set_rules('phonem', 'Phone Mobile', 'required');
            $this->form_validation->set_rules('addone', 'Address One', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please fill all the required field');
                redirect('business/edit/' . $enid);
            } else {
                $uid = $this->uid;
                $bname = $this->input->post('bname');
                $regnum = $this->input->post('regnum');
                $cname = $this->input->post('cname');
                $country = str_replace("string:", '', $this->input->post('country'));
                $phoneg = $this->input->post('phoneg');
                $phonem = $this->input->post('phonem');
                $addone = $this->input->post('addone');
                $addtwo = $this->input->post('addtwo');

                $eid = substr(sha1($uid), 0, 6);
                $img_path = wordwrap(strtolower($bname), 1, '-', 0);
                $path_one = './uploads/business/' . $eid . '/' . $img_path . '/';
                $path_rpl = str_replace(".", "", $path_one);
                $path_final = substr($path_rpl, 1);

                if (!file_exists($path_one)) {
                    mkdir($path_one, 0777, TRUE);
                }
                $config = array(
                    'upload_path' => $path_one,
                    'allowed_types' => "gif|jpg|png|jpeg",
                    'overwrite' => TRUE,
                    'max_size' => "2048",
                    'max_height' => "768",
                    'max_width' => "1024",
                    'overwrite' => TRUE
                );

                $this->load->library('upload', $config);
                $path_name = '';
                if ($this->upload->do_upload()) {
                    $data = $this->upload->data();
                    $path_name = $path_final . $data['file_name'];
                }

                $fpath = '';
                $bd = $this->business->get_business($id);
                if (!empty($path_name)) {
                    $fpath = $path_name;
                } else {
                    $fpath = $bd[0]['business_logo'];
                }

                $this->business->update_business($id, $bname, $regnum, $cname, $country, $phoneg, $phonem, $addone, $addtwo, $fpath);
                $this->session->set_flashdata('succ_msg', 'Business successfully updated');
                redirect('business/edit/' . $enid);
            }
        } else {
            $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
            redirect('business/list-business');
        }
    }

    function get_business() {
        $id = $this->session->userdata('bsid');
        $data = $this->business->get_business($id);
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    function remove_session() {
        $this->session->unset_userdata('prdid');
    }

}
