<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stores extends CI_Controller {

    public $uid = 'arwghrr@gmail.com';

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function create() {
        $data['title'] = 'Create Store';
        $data['map'] = 'true';
        $this->load->view('stores/create', $data);
    }

    public function create_store() {
        $this->form_validation->set_rules('bname', 'Business Name', 'required');
        $this->form_validation->set_rules('sname', 'Store Name', 'required');
        $this->form_validation->set_rules('stype', 'Store Type', 'required');
        $this->form_validation->set_rules('pac', 'Location', 'required');
        $this->form_validation->set_rules('latbox', 'Latitude', 'required');
        $this->form_validation->set_rules('lngbox', 'longitude', 'required');
        $this->form_validation->set_rules('region', 'Region', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('err_msg', 'Please fill all the required field');
            redirect('stores/create');
        } else {
            $uid = $this->uid;
            $bname = str_replace("string:", '', $this->input->post('bname'));
            $sname = $this->input->post('sname');
            $stype = str_replace("string:", '', $this->input->post('stype'));
            $loc = $this->input->post('pac');
            $lat = $this->input->post('latbox');
            $lng = $this->input->post('lngbox');
            $region = $this->input->post('region');

            $ext = $this->stores->exist_store($bname, $sname, $stype);
            if (count($ext) == 0) {
                $this->stores->add_store($bname, $sname, $stype, $loc, $lat, $lng, $region, $uid);
                $this->session->set_flashdata('succ_msg', 'Store successfully created');
                redirect('stores/create');
            } else {
                $this->session->set_flashdata('err_msg', 'This Store already exists');
                redirect('stores/create');
            }
        }
    }

    public function list_stores() {
        $this->remove_session();
        $data['table'] = 'true';
        $data['st'] = $this->stores->all_stores($this->uid);
        $this->load->view('stores/list', $data);
    }

    public function edit() {
        $this->remove_session();
        $id = decrypt_url($this->uri->segment(3));
        $this->session->set_userdata('stid', $id);
        $data['stact'] = 'active';
        $this->load->view('stores/edit', $data);
    }

    public function update_store() {
        $id = $this->session->userdata('stid');
        $enid = encrypt_url($id);
        if (!empty($id)) {
            $this->form_validation->set_rules('bname', 'Business Name', 'required');
            $this->form_validation->set_rules('sname', 'Store Name', 'required');
            $this->form_validation->set_rules('stype', 'Store Type', 'required');
            $this->form_validation->set_rules('pac', 'Location', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please fill all the required field');
                redirect('stores/edit/' . $enid);
            } else {
                $bname = str_replace("string:", '', $this->input->post('bname'));
                $sname = $this->input->post('sname');
                $stype = str_replace("string:", '', $this->input->post('stype'));
                $loc = $this->input->post('pac');
                $latt = $this->input->post('latbox');
                $lngt = $this->input->post('lngbox');
                $rgn = $this->input->post('region');
                $dst = $this->input->post('district');
                $lat = 0;
                $lng = 0;
                $region = 0;
                $distr = 0;
                if (!empty($latt) && !empty($lngt) && !empty($rgn) && !empty($dst)) {
                    $lat = $latt;
                    $lng = $lngt;
                    $region = $rgn;
                    $distr = $dst;
                } else {
                    $lat = $this->session->userdata('lat');
                    $lng = $this->session->userdata('lng');
                    $region = $this->session->userdata('region');
                    $distr = $this->session->userdata('dst');
                }

                $this->stores->update_store($id, $bname, $sname, $stype, $loc, $lat, $lng, $region, $distr);
                $this->session->set_flashdata('succ_msg', 'Store successfully updated');
                redirect('stores/edit/' . $enid);
            }
        } else {
            $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
            redirect('business/list-stores');
        }
    }

    function get_business() {
        $data = $this->stores->get_business($this->uid);
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    function get_store() {
        $this->remove_coordinates();
        $id = $this->session->userdata('stid');
        $data = $this->stores->get_store($id);
        $lat = $data[0]['latitude'];
        $lng = $data[0]['longitude'];
        $region = $data[0]['region'];
        $dstr = $data[0]['district'];
        $this->session->set_userdata(array('lat' => $lat, 'lng' => $lng, 'region' => $region, 'dst' => $dstr));
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    function remove_session() {
        $this->session->unset_userdata('stid');
    }

    function remove_coordinates() {
        $this->session->unset_userdata('lat');
        $this->session->unset_userdata('lng');
        $this->session->unset_userdata('region');
        $this->session->unset_userdata('dst');
    }

}
