-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 16, 2020 at 10:26 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gangfy_stores`
--

-- --------------------------------------------------------

--
-- Table structure for table `business`
--

CREATE TABLE `business` (
  `id` int(11) NOT NULL,
  `business_name` varchar(150) NOT NULL,
  `reg_no` varchar(50) NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `country_id` int(11) NOT NULL,
  `phone_general` varchar(15) NOT NULL,
  `phone_mobile` varchar(15) NOT NULL,
  `address_one` varchar(255) NOT NULL,
  `address_two` varchar(255) DEFAULT NULL,
  `business_logo` varchar(255) DEFAULT NULL,
  `user_id` varchar(50) NOT NULL,
  `status_id` tinyint(3) NOT NULL DEFAULT '2',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `business`
--

INSERT INTO `business` (`id`, `business_name`, `reg_no`, `company_name`, `country_id`, `phone_general`, `phone_mobile`, `address_one`, `address_two`, `business_logo`, `user_id`, `status_id`, `created_at`) VALUES
(1, 'GangFy Stores', 'CA12728', 'GangFy Pvt Ltd', 2, '9411245678', '947689393', 'Toronto', 'Canada', 'uploads/business/22449c/gangfy-stores/Top-Logo-1.png', 'arwghrr@gmail.com', 2, '2020-04-08 17:18:05'),
(2, 'Sisira Stores', 'KA123', 'Sisira Pvt Ltd', 216, '9411245678', '947689393', 'No 60, Toradeniya', 'Madawala', 'uploads/business/22449c/sisira-stores/Top-Logo-1.png', 'arwghrr@gmail.com', 2, '2020-04-09 15:40:31');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `country` varchar(256) NOT NULL,
  `country_code` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `country`, `country_code`) VALUES
(1, 'Afghanistan', '+93'),
(2, 'Albania', '+355'),
(3, 'Algeria', '+213'),
(4, 'American Samoa', '+1-684'),
(5, 'Andorra', '+376'),
(6, 'Angola', '+244'),
(7, 'Anguilla', '+1-264'),
(8, 'Antarctica', '+672'),
(9, 'Antigua', '+1-268'),
(10, 'Argentina', '+54'),
(11, 'Armenia', '+374'),
(12, 'Aruba', '+297'),
(13, 'Ascension', '+247'),
(14, 'Australia', '+61'),
(15, 'Australian External Territories', '+672'),
(16, 'Austria', '+43'),
(17, 'Azerbaijan', '+994'),
(18, 'Bahamas', '+1-242'),
(19, 'Bahrain', '+973'),
(20, 'Bangladesh', '+880'),
(21, 'Barbados', '+1-246'),
(22, 'Barbuda', '+1-268'),
(23, 'Belarus', '+375'),
(24, 'Belgium', '+32'),
(25, 'Belize', '+501'),
(26, 'Benin', '+229'),
(27, 'Bermuda', '+1-441'),
(28, 'Bhutan', '+975'),
(29, 'Bolivia', '+591'),
(30, 'Bosnia & Herzegovina', '+387'),
(31, 'Botswana', '+267'),
(32, 'Brazil', '+55'),
(33, 'British Virgin Islands', '+1-284'),
(34, 'Brunei Darussalam', '+673'),
(35, 'Bulgaria', '+359'),
(36, 'Burkina Faso', '+226'),
(37, 'Burundi', '+257'),
(38, 'Cambodia', '+855'),
(39, 'Cameroon', '+237'),
(40, 'Canada', '+1'),
(41, 'Cape Verde Islands', '+238'),
(42, 'Cayman Islands', '+1-345'),
(43, 'Central African Republic', '+236'),
(44, 'Chad', '+235'),
(45, 'Chatham Island (New Zealand)', '+64'),
(46, 'Chile', '+56'),
(47, 'China (PRC)', '+86'),
(48, 'Christmas Island', '+61-8'),
(49, 'Cocos-Keeling Islands', '+61'),
(50, 'Colombia', '+57'),
(51, 'Comoros', '+269'),
(52, 'Congo', '+242'),
(53, 'Congo, Dem. Rep. of  (former Zaire)', '+243'),
(54, 'Cook Islands', '+682'),
(55, 'Costa Rica', '+506'),
(56, 'Côte d\'Ivoire (Ivory Coast)', '+225'),
(57, 'Croatia', '+385'),
(58, 'Cuba', '+53'),
(59, 'Cuba (Guantanamo Bay)', '+5399'),
(60, 'Curaçao', '+599'),
(61, 'Cyprus', '+357'),
(62, 'Czech Republic', '+420'),
(63, 'Denmark', '+45'),
(64, 'Diego Garcia', '+246'),
(65, 'Djibouti', '+253'),
(66, 'Dominica', '+1-767'),
(67, 'Dominican Republic', '+1-809 and'),
(68, 'East Timor', '+670'),
(69, 'Easter Island', '+56'),
(70, 'Ecuador', '+593'),
(71, 'Egypt', '+20'),
(72, 'El Salvador', '+503'),
(73, 'Equatorial Guinea', '+240'),
(74, 'Eritrea', '+291'),
(75, 'Estonia', '+372'),
(76, 'Ethiopia', '+251'),
(77, 'Falkland Islands (Malvinas)', '+500'),
(78, 'Faroe Islands', '+298'),
(79, 'Fiji Islands', '+679'),
(80, 'Finland', '+358'),
(81, 'France', '+33'),
(82, 'French Antilles', '+596'),
(83, 'French Guiana', '+594'),
(84, 'French Polynesia', '+689'),
(85, 'Gabonese Republic', '+241'),
(86, 'Gambia', '+220'),
(87, 'Georgia', '+995'),
(88, 'Germany', '+49'),
(89, 'Ghana', '+233'),
(90, 'Gibraltar', '+350'),
(91, 'Greece', '+30'),
(92, 'Greenland', '+299'),
(93, 'Grenada', '+1-473'),
(94, 'Guadeloupe', '+590'),
(95, 'Guam', '+1-671'),
(96, 'Guantanamo Bay', '+5399'),
(97, 'Guatemala', '+502'),
(98, 'Guinea-Bissau', '+245'),
(99, '', ''),
(100, 'Guinea', '+224'),
(101, 'Guyana', '+592'),
(102, 'Haiti', '+509'),
(103, 'Honduras', '+504'),
(104, 'Hong Kong', '+852'),
(105, 'Hungary', '+36'),
(106, 'Iceland', '+354'),
(107, 'India', '+91'),
(108, 'Indonesia', '+62'),
(109, 'Inmarsat (Atlantic Ocean - East)', '+871'),
(110, 'Inmarsat (Atlantic Ocean - West)', '+874'),
(111, 'Inmarsat (Indian Ocean)', '+873'),
(112, 'Inmarsat (Pacific Ocean)', '+872'),
(113, 'Iran', '+98'),
(114, 'Iraq', '+964'),
(115, 'Ireland', '+353'),
(116, 'Iridium', '+8816 - +8'),
(117, 'Israel', '+972'),
(118, 'Italy', '+39'),
(119, 'Jamaica', '+1-876'),
(120, 'Japan', '+81'),
(121, 'Jordan', '+962'),
(122, 'Kazakhstan', '+7'),
(123, 'Kenya', '+254'),
(124, 'Kiribati', '+686'),
(125, 'Korea (North)', '+850'),
(126, 'Korea (South)', '+82'),
(127, 'Kuwait', '+965'),
(128, 'Kyrgyz Republic', '+996'),
(129, 'Laos', '+856'),
(130, 'Latvia', '+371'),
(131, 'Lebanon', '+961'),
(132, 'Lesotho', '+266'),
(133, 'Liberia', '+231'),
(134, 'Libya', '+218'),
(135, 'Liechtenstein', '+423'),
(136, 'Lithuania', '+370'),
(137, 'Luxembourg', '+352'),
(138, 'Macao', '+853'),
(139, '', ''),
(140, 'Macedonia', '+389'),
(141, 'Madagascar', '+261'),
(142, 'Malawi', '+265'),
(143, 'Malaysia', '+60'),
(144, 'Maldives', '+960'),
(145, 'Mali Republic', '+223'),
(146, 'Malta', '+356'),
(147, 'Marshall Islands', '+692'),
(148, 'Martinique', '+596'),
(149, 'Mauritania', '+222'),
(150, 'Mauritius', '+230'),
(151, 'Mayotte Island', '+269'),
(152, 'Mexico', '+52'),
(153, 'Micronesia', '+691'),
(154, 'Midway Island', '+1-808'),
(155, 'Moldova', '+373'),
(156, 'Monaco', '+377'),
(157, 'Mongolia', '+976'),
(158, 'Montenegro', '+382'),
(159, 'Montserrat', '+1-664'),
(160, 'Morocco', '+212'),
(161, 'Mozambique', '+258'),
(162, 'Myanmar', '+95'),
(163, 'Namibia', '+264'),
(164, 'Nauru', '+674'),
(165, 'Nepal', '+977'),
(166, 'Netherlands', '+31'),
(167, 'Netherlands Antilles', '+599'),
(168, 'Nevis', '+1-869'),
(169, 'New Caledonia', '+687'),
(170, 'New Zealand', '+64'),
(171, 'Nicaragua', '+505'),
(172, 'Niger', '+227'),
(173, 'Nigeria', '+234'),
(174, 'Niue', '+683'),
(175, 'Norfolk Island', '+672'),
(176, 'Northern Marianas Islands', '+1-670'),
(177, 'Norway', '+47'),
(178, 'Oman', '+968'),
(179, '', ''),
(180, 'Pakistan', '+92'),
(181, 'Palau', '+680'),
(182, 'Palestinian Settlements', '+970'),
(183, 'Panama', '+507'),
(184, 'Papua New Guinea', '+675'),
(185, 'Paraguay', '+595'),
(186, 'Peru', '+51'),
(187, 'Philippines', '+63'),
(188, 'Poland', '+48'),
(189, 'Portugal', '+351'),
(190, 'Puerto Rico', '+1-787 or '),
(191, 'Qatar', '+974'),
(192, 'Réunion Island', '+262'),
(193, 'Romania', '+40'),
(194, 'Russia', '+7'),
(195, 'Rwandese Republic', '+250'),
(196, 'St. Helena', '+290'),
(197, 'St. Kitts/Nevis', '+1-869'),
(198, 'St. Lucia', '+1-758'),
(199, 'St. Pierre & Miquelon', '+508'),
(200, 'St. Vincent & Grenadines', '+1-784'),
(201, 'Samoa', '+685'),
(202, 'San Marino', '+378'),
(203, 'São Tomé and Principe', '+239'),
(204, 'Saudi Arabia', '+966'),
(205, 'Senegal', '+221'),
(206, 'Serbia', '+381'),
(207, 'Seychelles Republic', '+248'),
(208, 'Sierra Leone', '+232'),
(209, 'Singapore', '+65'),
(210, 'Slovak Republic', '+421'),
(211, 'Slovenia', '+386'),
(212, 'Solomon Islands', '+677'),
(213, 'Somali Democratic Republic', '+252'),
(214, 'South Africa', '+27'),
(215, 'Spain', '+34'),
(216, 'Sri Lanka', '+94'),
(217, 'Sudan', '+249'),
(218, 'Suriname', '+597'),
(220, 'Swaziland', '+268'),
(221, 'Sweden', '+46'),
(222, 'Switzerland', '+41'),
(223, 'Syria', '+963'),
(224, 'Taiwan', '+886'),
(225, 'Tajikistan', '+992'),
(226, 'Tanzania', '+255'),
(227, 'Thailand', '+66'),
(228, 'Timor Leste', '+670'),
(229, 'Togolese Republic', '+228'),
(230, 'Tokelau', '+690'),
(231, 'Tonga Islands', '+676'),
(232, 'Trinidad & Tobago', '+1-868'),
(233, 'Tunisia', '+216'),
(234, 'Turkey', '+90'),
(235, 'Turkmenistan', '+993'),
(236, 'Turks and Caicos Islands', '+1-649'),
(237, 'Tuvalu', '+688'),
(238, 'Uganda', '+256'),
(239, 'Ukraine', '+380'),
(240, 'United Arab Emirates', '+971'),
(241, 'United Kingdom', '+44'),
(242, 'United States of America', '+1'),
(243, 'US Virgin Islands', '+1-340'),
(244, 'Uruguay', '+598'),
(245, 'Uzbekistan', '+998'),
(246, 'Vanuatu', '+678'),
(247, 'Vatican City', '+39 or +37'),
(248, 'Venezuela', '+58'),
(249, 'Vietnam', '+84'),
(250, 'Wake Island', '+808'),
(251, 'Wallis and Futuna Islands', '+681'),
(252, 'Yemen', '+967'),
(253, 'Zambia', '+260'),
(254, 'Zanzibar', '+255'),
(255, 'Zimbabwe', '+263');

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `code` varchar(5) NOT NULL,
  `number` smallint(4) NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `code`, `number`, `status`) VALUES
(1, 'LKR', 144, 1),
(2, 'USD', 840, 1),
(3, 'CAD', 124, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `scale_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `min_quantity` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `price` double(10,2) NOT NULL,
  `pstatus_id` int(11) NOT NULL,
  `description` text,
  `image` text,
  `user_id` varchar(50) NOT NULL,
  `status_id` tinyint(3) NOT NULL DEFAULT '2',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `store_id`, `product_name`, `scale_id`, `quantity`, `min_quantity`, `currency_id`, `price`, `pstatus_id`, `description`, `image`, `user_id`, `status_id`, `created_at`) VALUES
(1, 1, 'Red Dhal', 2, 1, 2, 1, 70.00, 14, '', 'uploads/products/22449c/red-dhal/red-lentils.jpg', 'arwghrr@gmail.com', 2, '2020-04-15 16:59:32'),
(2, 1, 'Onion', 2, 1, 2, 1, 130.00, 12, '<p>Onion</p>', 'uploads/products/22449c/onion/onion.jpeg', 'arwghrr@gmail.com', 2, '2020-04-16 14:12:32');

-- --------------------------------------------------------

--
-- Table structure for table `scale`
--

CREATE TABLE `scale` (
  `id` int(11) NOT NULL,
  `scale` varchar(15) NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scale`
--

INSERT INTO `scale` (`id`, `scale`, `status`) VALUES
(1, 'Item', 1),
(2, 'Kilogram', 1),
(3, 'Gram', 1),
(4, 'Liter', 1),
(5, 'Milliliter', 1),
(6, 'Bag', 1),
(7, 'Package', 1);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `level` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status`, `level`) VALUES
(1, 'Active', 1),
(2, 'Pending', 1),
(3, 'Deactivate', 1),
(4, 'New', 1),
(5, 'Deleted', 3),
(6, 'Paid', 1),
(7, 'Pending Approval', 1),
(8, 'Approved', 1),
(9, 'Closed', 1),
(10, 'Canceled', 1),
(11, 'Settled', 1),
(12, 'Available', 2),
(13, 'Not Available', 2),
(14, 'Sold Out', 2),
(15, 'Out of Stock', 2);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `store_name` varchar(100) NOT NULL,
  `st_id` int(11) NOT NULL,
  `store_location` text NOT NULL,
  `region` varchar(50) NOT NULL,
  `district` varchar(50) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `status_id` tinyint(3) NOT NULL DEFAULT '2',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `business_id`, `store_name`, `st_id`, `store_location`, `region`, `district`, `latitude`, `longitude`, `user_id`, `status_id`, `created_at`) VALUES
(1, 1, 'GangFy Grocery', 2, 'SOFF Indoor Cricket Facility, Sismet Road, Mississauga, ON, Canada', 'Ontario', 'Regional Municipality of Peel', 43.6460582, -79.61950449999999, 'arwghrr@gmail.com', 2, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `store_types`
--

CREATE TABLE `store_types` (
  `id` int(11) NOT NULL,
  `store_type` varchar(20) NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `store_types`
--

INSERT INTO `store_types` (`id`, `store_type`, `status`) VALUES
(1, 'All', 1),
(2, 'Grocery', 1),
(3, 'Pharmacy', 1),
(4, 'Agri', 1),
(5, 'Foods', 1),
(6, 'Stationery', 1),
(7, 'Hardware', 1),
(8, 'Electronics', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `business`
--
ALTER TABLE `business`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scale`
--
ALTER TABLE `scale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_types`
--
ALTER TABLE `store_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `business`
--
ALTER TABLE `business`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=256;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scale`
--
ALTER TABLE `scale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `store_types`
--
ALTER TABLE `store_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
